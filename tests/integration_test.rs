use std::collections::HashMap;

use tiny_serde::{Serialize, Value};

#[derive(Serialize)]
pub struct Thing;

#[derive(Serialize)]
struct Thing2;

#[derive(Serialize)]
pub struct Thing3 {}

#[derive(Serialize)]
struct Thing4 {}

#[derive(Serialize)]
struct Thing5 {
    name: String,
}

#[derive(Serialize)]
struct Thing6 {
    name: HashMap<String, Value>,
}

#[derive(Serialize)]
struct Thing7(HashMap<String, Value>);

#[test]
fn test() {
    let thing = Thing;
    let _ = thing;
    let thing5 = Thing5 {
        name: "Hello slt \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\".to_string(),
    };
    println!("Thing5 : {:#?}", thing5.serialize());
}
