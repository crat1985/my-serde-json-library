A tiny serde_json alternative with no dependencies.

**This library is still WIP**

# Features

- [x] Serialization from Value to JSON String
  - [x] Basic implementation
  - [x] Patch issue with " and \ in Value
- [x] Deserialization from JSON String to Value
- [x] Types
  - [x] String
  - [x] Array
  - [x] Object
  - [x] Number
  - [x] Boolean
  - [x] Null
- [x] Traits
  - [x] Serialize
  - [x] Deserialize
- [ ] Derive macros
  - [x] Serialize
    - [x] Basic implementation
  - [ ] Deserialize
