use std::collections::HashMap;

use crate::{Number, Value};

pub trait Serialize {
    fn serialize(&self) -> Value;
}

pub fn to_string<S: Serialize>(value: &S) -> String {
    value.serialize().to_string()
}

impl Serialize for String {
    fn serialize(&self) -> Value {
        Value::String(self.clone())
    }
}

impl<S: Serialize> Serialize for Option<S> {
    fn serialize(&self) -> Value {
        match self {
            Some(value) => value.serialize(),
            None => Value::Null,
        }
    }
}

impl Serialize for bool {
    fn serialize(&self) -> Value {
        Value::Boolean(*self)
    }
}

impl Serialize for f64 {
    fn serialize(&self) -> Value {
        Value::Number(Number::Decimal(*self))
    }
}

impl Serialize for u128 {
    fn serialize(&self) -> Value {
        Value::Number(Number::Integer(*self))
    }
}

impl Serialize for i128 {
    fn serialize(&self) -> Value {
        Value::Number(Number::SignedInteger(*self))
    }
}

impl Serialize for Vec<Value> {
    fn serialize(&self) -> Value {
        Value::Array(self.clone())
    }
}

impl Serialize for HashMap<String, Value> {
    fn serialize(&self) -> Value {
        Value::Object(self.clone())
    }
}
