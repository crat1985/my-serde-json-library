#[derive(Debug)]
pub enum SerDeJsonError {
    MissingField {
        field: String,
    },
    UnexpectedEnd {
        index: usize,
    },
    InvalidJson {
        index: usize,
    },
    DupplicateKey {
        key: String,
        index: usize,
    },
    NumberError {
        number: String,
        index: usize,
        error: String,
    },
}
